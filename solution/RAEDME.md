
- Create bastion host 

![host-entry](./1.png)
![host-entry](./2.png)
![host-entry](./3.png)

- Create Pvt ec2

![host-entry](./4.png)
![host-entry](./5.png)
![host-entry](./6.png)

- access to pvt-ec2 from bastion host 

![host-entry](./7.png)
![host-entry](./8.png)
![host-entry](./9.png)
![host-entry](./10.png)